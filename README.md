# Time Of Day Plugin

**This README.md file should be modified to describe the features, installation, configuration, and general usage of the plugin.**

The **Time Of Day** Plugin is an extension for [Grav CMS](http://github.com/getgrav/grav). Reads Markdown shortcode `{TOD}` and inserts "morning", "afternoon", "evening", or "night" as appropriate.

## Installation

Installing the Time Of Day plugin can be done in one of three ways: The GPM (Grav Package Manager) installation method lets you quickly install the plugin with a simple terminal command, the manual method lets you do so via a zip file, and the admin method lets you do so via the Admin Plugin.

### GPM Installation (Preferred)

To install the plugin via the [GPM](http://learn.getgrav.org/advanced/grav-gpm), through your system's terminal (also called the command line), navigate to the root of your Grav-installation, and enter:

    bin/gpm install time-of-day

This will install the Time Of Day plugin into your `/user/plugins`-directory within Grav. Its files can be found under `/your/site/grav/user/plugins/time-of-day`.

### Manual Installation

To install the plugin manually, download the zip-version of this repository and unzip it under `/your/site/grav/user/plugins`. Then rename the folder to `time-of-day`. You can find these files on [GitHub](https://github.com//grav-plugin-time-of-day) or via [GetGrav.org](http://getgrav.org/downloads/plugins#extras).

You should now have all the plugin files under

    /your/site/grav/user/plugins/time-of-day
	
> NOTE: This plugin is a modular component for Grav which may require other plugins to operate, please see its [blueprints.yaml-file on GitHub](https://github.com//grav-plugin-time-of-day/blob/master/blueprints.yaml).

### Admin Plugin

If you use the Admin Plugin, you can install the plugin directly by browsing the `Plugins`-menu and clicking on the `Add` button.

## Configuration

Before configuring this plugin, you should copy the `user/plugins/time-of-day/time-of-day.yaml` to `user/config/plugins/time-of-day.yaml` and only edit that copy.

Here is the default configuration and an explanation of available options:

```yaml
enabled: true
```

Note that if you use the Admin Plugin, a file with your configuration named time-of-day.yaml will be saved in the `user/config/plugins/`-folder once the configuration is saved in the Admin.

## Usage

To use simply add `{TOD}` into your markdown files. 

`#Good {TOD}`

The above should return "Good morning", "Good afternoon", "Good evening", or "Good night", depending on the timeof day (according to PHP date()).

By default;
- it is considered "morning" between 4am and 12pm. 
- it is considered "afternoon" between 12pm and 5pm. 
- it is considered "evening" between 5pm and 9pm. 
- it is considered "night" between 9pm and 4am. 

You can change these hours and responses relatively easily.

## Credits

Thank you to the grav community. Only a few hours in and I am able to make a somewhat useful plugin. Kudos for a great system!

## To Do

- [ ] Proper settings for hours and responses
- [ ] Add CSS class and wrapper span to response for styling
- [ ] Language support ?
- [ ] Add to GitHub for inclusion in grav directory

