<?php
namespace Grav\Plugin;

use Composer\Autoload\ClassLoader;
use Grav\Common\Plugin;

/**
 * Class TimeOfDayPlugin
 * @package Grav\Plugin
 */
class TimeOfDayPlugin extends Plugin
{

     public static function getSubscribedEvents() {
        return [
            'onPluginsInitialized' => ['onPluginsInitialized', 0],
        ];
    }

    public function onPluginsInitialized()
    {
        if ($this->isAdmin()) {
            return;
        }
        $this->enable([
            'onPageContentRaw' => ['onPageContentRaw', 0],
        ]);
    }

    public function onPageContentRaw($e)
    {
        $content = $e['page']->getRawContent();
        $currentHour = intval(date("H"));
        switch($currentHour){
            case (($currentHour >= 4) && ($currentHour < 12)) :
                 $tod = 'morning';
                 break;
            case (($currentHour >= 12) && ($currentHour < 17)) :
                $tod = 'afternoon';
                break;
            case (($currentHour >=17) && ($currentHour < 21)) :
                $tod = 'evening';
                break;
            case (($currentHour >= 21) && ($currentHour < 4)) :
                $tod = 'night';
                break;
            default : 
                $tod = 'day';
        }
        $found = strpos($content, '{TOD}');
        if($found){
            $content = str_replace('{TOD}', $tod, $content);
        }
        $e['page']->setRawContent($content);
    }
}
